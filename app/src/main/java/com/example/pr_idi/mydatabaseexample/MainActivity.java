package com.example.pr_idi.mydatabaseexample;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {
    private BookData bookData;
    Book selectedFromList;
    int elemSeleccionat;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayAdapter<String> mAdapter;
    private List<Book> booksList = new ArrayList<>();

    private ArrayAdapter<Book> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final ListView lv = (ListView) findViewById(android.R.id.list);

        bookData = new BookData(this);
        bookData.open();

        booksList = bookData.getAllBooks();

        mDrawerList = (ListView)findViewById(R.id.navList);

        addDrawerItems();

        bookData.close();
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, booksList);
        //Sort adapter by title;
        adapter.sort(new Comparator<Book>() {
            @Override
            public int compare(Book lhs, Book rhs) {
                return lhs.getTitle().compareToIgnoreCase(rhs.getTitle());
            }
        });
        lv.setAdapter(adapter);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle("MyBook");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("MyBook");
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

      /*  lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                if(v.getDrawingCacheBackgroundColor() == Color.rgb(191,157,0)){
                    v.setBackgroundColor(Color.BLACK);
                }
                else v.setBackgroundColor(Color.rgb(191,157,0));
                adapter.isEnabled(position);
                selectedFromList =(Book) (a.getItemAtPosition(position));
                elemSeleccionat = position;
                Toast.makeText(getApplicationContext(),"He seleccionado "+ selectedFromList.getTitle(),Toast.LENGTH_SHORT).show();
            }
        });*/
        registerForContextMenu(lv);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...
        switch (item.getItemId()) {
            case R.id.help:
                Intent intent = new Intent(this, HelpMain.class);
                startActivity(intent);
                return true;
            case R.id.about:
                Intent intent2 = new Intent(this, AboutActivity.class);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

       SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Títol de llibre");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add("Esborrar llibre");
        SubMenu sub = menu.addSubMenu("Editar valoració");
        sub.add("Molt bo");
        sub.add("Bo");
        sub.add("Regular");
        sub.add("Dolent");
        sub.add("Molt dolent");
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final List<Book> values = bookData.getAllBooks();
        final ArrayAdapter<Book> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, values);
        if(item.getTitle() == "Molt bo") {
            selectedFromList.setPersonal_evaluation("Molt bo");
            bookData.updateLlibre(selectedFromList);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Valoració de " + selectedFromList.getTitle()+ " canviada a: " + selectedFromList.getPersonal_evaluation(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getTitle() == "Bo") {
            selectedFromList.setPersonal_evaluation("Bo");
            bookData.updateLlibre(selectedFromList);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Valoració de " + selectedFromList.getTitle()+ " canviada a: " + selectedFromList.getPersonal_evaluation(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getTitle() == "Regular") {
            selectedFromList.setPersonal_evaluation("Regular");
            bookData.updateLlibre(selectedFromList);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Valoració de " + selectedFromList.getTitle()+ " canviada a: " + selectedFromList.getPersonal_evaluation(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getTitle() == "Dolent") {
            selectedFromList.setPersonal_evaluation("Dolent");
            bookData.updateLlibre(selectedFromList);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Valoració de " + selectedFromList.getTitle()+ " canviada a: " + selectedFromList.getPersonal_evaluation(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getTitle() == "Molt dolent") {
            selectedFromList.setPersonal_evaluation("Molt dolent");
            bookData.updateLlibre(selectedFromList);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Valoració de " + selectedFromList.getTitle()+ " canviada a: " + selectedFromList.getPersonal_evaluation(), Toast.LENGTH_SHORT).show();
            return true;
        }
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        elemSeleccionat = info.position;
        ListView lv = (ListView) findViewById(android.R.id.list);
        selectedFromList = (Book) lv.getItemAtPosition(elemSeleccionat);
        if (item.getTitle() == "Editar valoració") {
            Menu menu = new MenuBuilder(this);
            menu.add("Molt bo");
            menu.add("Bo");
            menu.add("Regular");
            menu.add("Dolent");
            menu.add("Molt dolent");
            return true;
        }
        else if (item.getTitle() =="Esborrar llibre") {
            item.getItemId();
            AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
            b.setTitle("Esborrar llibre: " + selectedFromList.getTitle() + " - " + selectedFromList.getAuthor());
            b.setMessage("Estàs a punt d'esborrar un llibre permanentment. Vols continuar?");
            b.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    bookData.deleteBook(selectedFromList);
                    bookData.close();
                    adapter.remove(selectedFromList);
                    adapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "S'ha eliminat "+ selectedFromList.getTitle() + " " + selectedFromList.getAuthor(), Toast.LENGTH_SHORT).show();
                    changeActivity(2);
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Nothing
                }
            });
            AlertDialog alertDialog = b.create();
            alertDialog.show();
            return true;
        }
        else return false;
    }

        private void addDrawerItems() {
        String[] osArray = { "Afegir llibre", "Llista detallada de llibres", "Buscar llibres per autor"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        changeActivity(0);
                        break;
                    case 1:
                        changeActivity(1);
                        break;
                    case 2:
                        changeActivity(3);
                        break;
                }
            }
        });
    }

    private void changeActivity(int activity) {
        switch (activity) {
            case 0:
                Intent intent = new Intent(this, AltaLlibre.class);
                startActivity(intent);
                break;
            case 1:
                Intent intent2 = new Intent(this, RecyclerViewList.class);
                startActivity(intent2);
                break;
            case 2:
                Intent intent3 = new Intent(this, MainActivity.class);
                startActivity(intent3);
                break;
            case 3:
                Intent intent4 = new Intent(this, SearchAutor.class);
                startActivity(intent4);
                break;
        }

    }

    // Basic method to add pseudo-random list of books so that
    // you have an example of insertion and deletion

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
     /**   @SuppressWarnings("unchecked")
        ArrayAdapter<Book> adapter = (ArrayAdapter<Book>) getListAdapter();
        ListView lv = (ListView) findViewById(android.R.id.list);
        Book book;
        switch (view.getId()) {
            case R.id.add:
                String[] newBook = new String[] { "Miguel Strogoff", "Jules Verne", "Ulysses", "James Joyce", "Don Quijote", "Miguel de Cervantes", "Metamorphosis", "Kafka" };
                int nextInt = new Random().nextInt(4);
                // save the new book to the database
                book = bookData.createBook(newBook[nextInt*2], newBook[nextInt*2 + 1]);
                // After I get the book data, I add it to the adapter
                adapter.add(book);

                break;
            case R.id.delete:
                Book llibre = (Book) lv.getItemAtPosition(elemSeleccionat);
                bookData.deleteBook(llibre);
                adapter.remove(llibre);
                break;
            case R.id.buttonAltaLlibre:
                Intent intent = new Intent(this, AltaLlibre.class);
                startActivity(intent);
                break;
            case R.id.rec:
                Intent intent2 = new Intent(this, RecyclerViewList.class);
                startActivity(intent2);
                break;
        }
        adapter.notifyDataSetChanged();*/
    }

    @Override
    protected void onResume() {
        bookData.open();
        super.onResume();
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onPause() {
        bookData.close();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT); //CLOSE Nav Drawer!
        }
    }

}