package com.example.pr_idi.mydatabaseexample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AltaLlibre extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private BookData bookData;
    EditText titolaux, autoraux, anyPaux, editorialaux, categoriaaux;
    Spinner valoracioPaux;
    Button addButton, cancelButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atla_bo);
        bookData = new BookData(this);
        bookData.open();
        getSupportActionBar().setTitle("Afegir llibre");
        titolaux = (EditText) findViewById(R.id.editTextTitol);
        autoraux = (EditText) findViewById(R.id.editTextAutor);
        anyPaux = (EditText) findViewById(R.id.editTextAnyP);
        editorialaux = (EditText) findViewById(R.id.editTextEditorial);
        categoriaaux = (EditText) findViewById(R.id.editTextCategoria);
        valoracioPaux = (Spinner)  findViewById(R.id.spinnerVP);
        addButton = (Button) findViewById(R.id.addButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (validate()) {

                    String resTitol = String.valueOf(titolaux.getText());
                    String resAutor = String.valueOf(autoraux.getText());
                    String resAnyP = anyPaux.getText().toString();
                    String resEditorial = String.valueOf(editorialaux.getText());
                    String resCategoria = String.valueOf(categoriaaux.getText());
                    String resValoracioP = String.valueOf(valoracioPaux.getSelectedItem());

                    Book book = new Book();
                    book.setTitle(resTitol);
                    book.setAuthor(resAutor);
                    book.setYear(Integer.parseInt(resAnyP));
                    book.setPublisher(resEditorial);
                    book.setCategory(resCategoria);
                    book.setPersonal_evaluation(resValoracioP);
                    // save the new book to the database
                    bookData.altaLlibre(book);
                    Toast.makeText(getApplicationContext(),"Llibre donat d'alta: " + resTitol,Toast.LENGTH_SHORT).show();
                    bookData.close();
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(checkFieldsAreEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AltaLlibre.this);
                    alertDialogBuilder.setTitle("Canvis sense guardar");
                    alertDialogBuilder.setMessage("Els canvis realitzats no es guardaran. Vols continuar?");
                    alertDialogBuilder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    finish();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else{
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_others, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if(checkFieldsAreEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AltaLlibre.this);
                    alertDialogBuilder.setTitle("Canvis sense guardar");
                    alertDialogBuilder.setMessage("Els canvis realitzats no es guardaran. Vols continuar?");
                    alertDialogBuilder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    finish();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else {
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.help:
                if(checkFieldsAreEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AltaLlibre.this);
                    alertDialogBuilder.setTitle("Canvis sense guardar");
                    alertDialogBuilder.setMessage("Els canvis realitzats no es guardaran. Vols continuar?");
                    alertDialogBuilder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    finish();
                                    Intent intent = new Intent(getApplicationContext(), HelpAltaLlibre.class);
                                    startActivity(intent);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else {
                    finish();
                    Intent intent = new Intent(getApplicationContext(), HelpAltaLlibre.class);
                    startActivity(intent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public boolean checkFieldsAreEmpty()
    {
        boolean areEmpty = true;
        if(titolaux.getText().toString().length() == 0 && autoraux.getText().toString().length() == 0 &&
                anyPaux.getText().toString().length() == 0 && editorialaux.getText().toString().length() == 0 &&
                categoriaaux.getText().toString().length() == 0 && valoracioPaux.getSelectedItem().toString().equals("Escull valoració")){
            areEmpty = false;
        }
        return areEmpty;
    }

    public boolean validate()
    {
        boolean isValid = true;

        if(titolaux.getText().toString().length() <= 0){
            titolaux.setError("S'ha d'omplir el camp");
            isValid = false;
        }
        if(autoraux.getText().toString().trim().equalsIgnoreCase("")){
            autoraux.setError("S'ha d'omplir el camp");
            isValid = false;
        }
        if(anyPaux.getText().toString().trim().equalsIgnoreCase("") || Integer.parseInt(anyPaux.getText().toString().trim()) > 2017){
            anyPaux.setError("S'ha d'omplir el camp.Introduir data vàlida.");
            isValid = false;
        }
        if(editorialaux.getText().toString().trim().equalsIgnoreCase("")){
            editorialaux.setError("S'ha d'omplir el camp");
            isValid = false;
        }
        if(categoriaaux.getText().toString().trim().equalsIgnoreCase("")){
            categoriaaux.setError("S'ha d'omplir el camp");
            isValid = false;
        }
        View selectedView = valoracioPaux.getSelectedView();
        TextView selectedTextView = (TextView) selectedView;
        if(valoracioPaux.getSelectedItem().toString().equals("Escull valoració")){
            selectedTextView.setError("Escull valoració");
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onBackPressed() {
        if(checkFieldsAreEmpty()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AltaLlibre.this);
            alertDialogBuilder.setTitle("Canvis sense guardar");
            alertDialogBuilder.setMessage("Els canvis realitzats no es guardaran. Vols continuar?");
            alertDialogBuilder.setPositiveButton("Si",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    });

            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Do nothing
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        else {
            finish();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

}
