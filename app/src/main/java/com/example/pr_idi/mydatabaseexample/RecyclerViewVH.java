package com.example.pr_idi.mydatabaseexample;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Maria on 26/12/2016.
 */

public class RecyclerViewVH extends RecyclerView.ViewHolder {
    TextView textViewTitol;
    TextView textViewAutor;
    TextView textViewAnyPublicacio;
    TextView textViewEditorial;
    TextView textViewCategoria;
    TextView textViewValoracioPersonal;

    public RecyclerViewVH(View itemView) {
        super(itemView);
        textViewTitol = (TextView) itemView.findViewById(R.id.titol);
        textViewAutor = (TextView) itemView.findViewById(R.id.autor);
        textViewAnyPublicacio = (TextView) itemView.findViewById(R.id.anyPublicacio);
        textViewEditorial = (TextView) itemView.findViewById(R.id.editorial);
        textViewCategoria = (TextView) itemView.findViewById(R.id.categoria);
        textViewValoracioPersonal = (TextView) itemView.findViewById(R.id.valoracioPersonal);
    }
}

