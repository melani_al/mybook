package com.example.pr_idi.mydatabaseexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Maria on 22/12/2016.
 */

public class RecyclerViewList extends AppCompatActivity {
    private BookData bookData;
    private List<Book> booksList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecycleViewAdapter mAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        getSupportActionBar().setTitle("Llista detallada de llibres");
        recyclerView = (RecyclerView) findViewById(R.id.myrecyclerview);
        bookData = new BookData(this);
        bookData.open();
        booksList = bookData.getAllBooks();
        bookData.close();
        Collections.sort(booksList, new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getCategory().compareToIgnoreCase(o2.getCategory());
            }
        });
        mAdapter = new RecycleViewAdapter(booksList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new RecyclerBarraDecoration(this));
        recyclerView.setAdapter(mAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_others, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.help:
                Intent intent1 = new Intent(this, HelpRecycler.class);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
