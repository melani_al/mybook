package com.example.pr_idi.mydatabaseexample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/**
 * Created by Maria on 24/12/2016.
 */


public class RecycleViewAdapter extends RecyclerView.Adapter<RecyclerViewVH>  {
    private List<Book> booksList;

    public RecycleViewAdapter(List<Book> booksList) {
        this.booksList = booksList;
    }

    @Override
    public RecyclerViewVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_recycler_view, parent, false);

        return new RecyclerViewVH(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewVH holder, int position) {
        Book book = booksList.get(position);
        holder.textViewTitol.setText(book.getTitle());
        holder.textViewAutor.setText(book.getAuthor());
        holder.textViewEditorial.setText(book.getPublisher());
        holder.textViewAnyPublicacio.setText(""+book.getYear());
        holder.textViewCategoria.setText(book.getCategory());
        holder.textViewValoracioPersonal.setText(book.getPersonal_evaluation());
    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }
}
